﻿using System;
using System.Collections.Generic;
using Piotr.RedisWebApi.Models;
using ServiceStack.Redis;

namespace Piotr.RedisWebApi.Data
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly IRedisClient _redisClient;

        public CustomerRepository(IRedisClient redisClient)
        {
            _redisClient = redisClient;
        }

        public IList<Customer> GetAll()
        {
            using (var typedClient = _redisClient.GetTypedClient<Customer>())
            {
                return typedClient.GetAll();
            }
        }

        public Customer Get(Guid id)
        {
            using (var typedClient = _redisClient.GetTypedClient<Customer>())
            {
                return typedClient.GetById(id);
            }
        }

        public Customer Store(Customer customer)
        {
            using (var typedClient = _redisClient.GetTypedClient<Customer>())
            {
                if (customer.Id == default(Guid))
                {
                    customer.Id = Guid.NewGuid();
                }
                return typedClient.Store(customer);
            }
        }
    }
}